package org.satreci.snap.pcs.actions.moonlight;

import org.esa.snap.core.dataio.ProductIO;
import org.esa.snap.core.datamodel.ProductData;
import org.esa.snap.rcp.SnapApp;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;
import org.satreci.snap.dataio.bin.BinProductReader;
import org.satreci.snap.pcs.ui.InputFileChooser;
import org.satreci.snap.pcs.py.ProcessWrapper;
import org.satreci.snap.pcs.py.PyInterface;
import org.satreci.snap.pcs.ui.modal.PCSProcessingDialog;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.nio.ByteOrder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

@ActionID(category = "GOCI-II", id = "org.satreci.snap.soe.ml.MoonLightVerificationAction")
@ActionRegistration(displayName = "#CTL_MoonLightVerificationActionName")
@ActionReference(path = "Menu/GOCI-II", position = 600)
@NbBundle.Messages("CTL_MoonLightVerificationActionName=월광 검정")
public class MoonLightVerificationAction extends AbstractAction implements InputFileChooser.SubmitListener, ProcessWrapper.FinishListener {
    private JTextField upperLeftX, upperLeftY, lowerRightY, lowerRightX;

    @Override
    public void actionPerformed(ActionEvent e) {
        PCSProcessingDialog.createDefaultDialog(new RoloOp.Spi(), Bundle.CTL_MoonLightVerificationActionName(), SnapApp.getDefault().getAppContext()).show();

        /*InputFileChooser chooser = new InputFileChooser(SnapApp.getDefault().getMainFrame(), Bundle.CTL_MoonLightVerificationActionName(), this);
        chooser.addInputItem("FDS Data", true, (file) -> checkFDSData(file));
        chooser.addInputItem("달 영상", true, (file) -> checkMoonImageFile(file));
        chooser.addInputItem("달 영상 헤더", true, (file) -> checkMoonImageHdrFile(file));
        chooser.addInputItem("Header", true, (file) -> checkHeaderFile(file));
        chooser.addInputField("DN threshold", InputFileChooser.InputType.NUMBER);
        chooser.addCustomView(createPositionInputForm());
        chooser.setVisible(true);*/
    }

    private JPanel createPositionInputForm() {
        JPanel content = new JPanel(new GridLayout(0, 4));
        content.setBorder(new EmptyBorder(10, 20, 0, 20));
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.getDefault());
        DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
        decimalFormat.setGroupingUsed(false);
        upperLeftX = new JFormattedTextField(decimalFormat);
        upperLeftY = new JFormattedTextField(decimalFormat);
        lowerRightX = new JFormattedTextField(decimalFormat);
        lowerRightY = new JFormattedTextField(decimalFormat);
        upperLeftX.setText("0");
        upperLeftY.setText("0");
        lowerRightX.setText("0");
        lowerRightY.setText("0");
        content.add(new Label("UpperLeftX"));
        content.add(upperLeftX);
        content.add(new Label("UpperLeftY"));
        content.add(upperLeftY);
        content.add(new Label("LowerRightX"));
        content.add(lowerRightX);
        content.add(new Label("LowerRightY"));
        content.add(lowerRightY);
        return content;
    }

    private boolean checkFDSData(File file) {
        return true;
    }

    private boolean checkMoonImageFile(File file) {
        return true;
    }

    private boolean checkMoonImageHdrFile(File file) {
        return true;
    }

    private boolean checkHeaderFile(File file) {
        return true;
    }

    private int getUpperLeftX() {
        return Integer.valueOf(upperLeftX.getText());
    }

    private int getUpperLeftY() {
        return Integer.valueOf(upperLeftY.getText());
    }

    private int getLowerRightX() {
        return Integer.valueOf(lowerRightX.getText());
    }

    private int getLowerRightY() {
        return Integer.valueOf(lowerRightY.getText());
    }

    @Override
    public void onSubmit(Object[] results) {
        File fdsData = (File) results[0];
        File moonPxl = (File) results[1];
        File moonHdr = (File) results[2];
        File header = (File) results[3];
        int dnThreshold = (int) results[4];
        String ulx = upperLeftX.getText();
        String uly = upperLeftY.getText();
        String lrx = lowerRightX.getText();
        String lry = lowerRightY.getText();
        PyInterface.createProcessBuilder("roloMain", fdsData, moonPxl, moonHdr, header, dnThreshold, ulx, uly, lrx, lry).start(Bundle.CTL_MoonLightVerificationActionName(), this);
    }

    @Override
    public void end(String[] outputs) {
        BinProductReader reader = ((BinProductReader) ProductIO.getProductReader("Binary product"));
        reader.initBandReader(getLowerRightY() - getUpperLeftY(), getLowerRightX() - getUpperLeftX(), Double.SIZE, ProductData.TYPE_FLOAT64, 1, ByteOrder.LITTLE_ENDIAN);
        /*OutputProcessingDialog dialog = new OutputProcessingDialog(SnapApp.getDefault().getMainFrame(), Bundle.CTL_MoonLightVerificationActionName());
        dialog.addProduct(new File(PCSOptionsUtil.getWorkingDir() + outputs[0]), reader);
        dialog.setVisible(true);*/

        String[][] tableData = new String[outputs.length][2];
        int count = 0;
        tableData[count] = new String[]{"moonIrradiance", outputs[++count]};
        tableData[count] = new String[]{"countMoonPixels", outputs[++count]};
        tableData[count] = new String[]{"sumMoonPixels", outputs[++count]};
        tableData[count] = new String[]{"LunarAgingFactor", outputs[++count]};
        tableData[count] = new String[]{"SlopeFactor", outputs[++count]};
        new ResultTableDialog(SnapApp.getDefault().getMainFrame(), tableData).setVisible(true);
    }

    @Override
    public void fail(String message) {
        InputFileChooser.showErrorDialog(message, Bundle.CTL_MoonLightVerificationActionName());
    }
}