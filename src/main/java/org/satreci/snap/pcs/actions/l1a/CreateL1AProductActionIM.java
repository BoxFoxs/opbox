package org.satreci.snap.pcs.actions.l1a;

import org.esa.snap.rcp.SnapApp;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;
import org.satreci.snap.pcs.ui.modal.PCSProcessingDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;

@ActionID(category = "GOCI2", id = "org.satreci.snap.soe.l1a.CreateL1AIMProductAction")
@ActionRegistration(displayName = "#CTL_CreateL1AIMProductActionName")
@ActionReference(path = "Menu/GOCI-II/L1A", position = 600)
@NbBundle.Messages("CTL_CreateL1AIMProductActionName=L1A 영상 생성(Inverse)")
public class CreateL1AProductActionIM extends AbstractAction {

    @Override
    public void actionPerformed(ActionEvent e) {
        PCSProcessingDialog.createDefaultDialog(new CreateL1AProductOp.IMSpi(), Bundle.CTL_CreateL1AIMProductActionName(), SnapApp.getDefault().getAppContext()).show();
    }
}