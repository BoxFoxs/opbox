package org.satreci.snap.pcs.actions.lv0;

import org.esa.snap.rcp.SnapApp;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;
import org.satreci.snap.pcs.ui.modal.PCSProcessingDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;

@ActionID(category = "GOCI-II", id = "org.satreci.snap.soe.lv0.VideoPixelDecompositionAction")
@ActionRegistration(displayName = "#CTL_VideoPixelDecompositionActionName")
@ActionReference(path = "Menu/GOCI-II/L0", position = 600)
@NbBundle.Messages("CTL_VideoPixelDecompositionActionName=Video Pixel 분해")
public class VideoPixelDecompositionAction extends AbstractAction {

    @Override
    public void actionPerformed(ActionEvent e) {
        PCSProcessingDialog.createDefaultDialog(new VideoPixelDecompositionOp.Spi(), Bundle.CTL_VideoPixelDecompositionActionName(), SnapApp.getDefault().getAppContext()).show();
    }

}
