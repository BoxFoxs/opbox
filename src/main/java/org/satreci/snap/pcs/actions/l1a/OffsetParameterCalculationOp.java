package org.satreci.snap.pcs.actions.l1a;

import com.bc.ceres.core.ProgressMonitor;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.Operator;
import org.esa.snap.core.gpf.OperatorException;
import org.esa.snap.core.gpf.annotations.OperatorMetadata;
import org.esa.snap.core.gpf.annotations.SourceProduct;
import org.satreci.snap.dataio.bin.ProductSize;
import org.satreci.snap.dataio.pcs.PCSProductReader;
import org.satreci.snap.pcs.preferences.PCSOptionsUtil;
import org.satreci.snap.pcs.py.ProcessWrapper;
import org.satreci.snap.pcs.py.PyInterface;
import org.satreci.snap.pcs.ui.InputFileChooser;
import org.satreci.snap.pcs.ui.modal.operator.annotation.TargetProduct;
import org.satreci.snap.pcs.ui.modal.operator.GOCIOperatorSpi;

import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;

@SuppressWarnings({"UnusedDeclaration", "MismatchedReadAndWriteOfArray, FieldCanBeLocal"})
@OperatorMetadata(alias = "L0VideoPixelOp",
        category = "GOCI-II",
        version = "0.0.1",
        authors = "Satreci",
        copyright = "",
        description = "GOCI-II Video Pixel Decomposition Processing")
public class OffsetParameterCalculationOp extends Operator implements ProcessWrapper.FinishListener {

    @SourceProduct(alias = "firstDark", label = "First Dark", description = "First Dark File")
    private Product firstDark;

    @SourceProduct(alias = "secondDark", label = "Second Dark", description = "Second Dark File")
    private Product secondDark;

    @TargetProduct(label = "Result")
    private Product targetProduct;

    @Override
    public void initialize() throws OperatorException {

    }

    @Override
    public void doExecute(ProgressMonitor pm) throws OperatorException {
        PyInterface.createProcessBuilder("L1aOffsetParameterMain", firstDark.getFileLocation(), secondDark.getFileLocation()).startBlocking(Bundle.CTL_OffsetParameterCalculationActionName(), this);
    }

    @Override
    public void end(String[] outputs) {
        try {
            PCSProductReader reader = new PCSProductReader("L1 Offset Parameter", 2720, 2718, ProductSize.DOUBLE, ByteOrder.LITTLE_ENDIAN);
            for (String output : outputs) {
                reader.addBand(new File(PCSOptionsUtil.getWorkingDir() + outputs));
            }
            targetProduct = reader.readProductNodes(null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fail(String msg) {
        InputFileChooser.showErrorDialog(msg, Bundle.CTL_OffsetParameterCalculationActionName());
    }

    public static class Spi extends GOCIOperatorSpi {

        public Spi() {
            super(OffsetParameterCalculationOp.class);
        }

    }
}