package org.satreci.snap.pcs.actions.dcc;

import org.esa.snap.rcp.SnapApp;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;
import org.satreci.snap.pcs.ui.modal.PCSProcessingDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;

@ActionID(category = "GOCI-II", id = "org.satreci.snap.soe.dcc.OffsetParameterCalculationAction")
@ActionRegistration(displayName = "#CTL_OffsetParameterCalculationActionName")
@ActionReference(path = "Menu/GOCI-II/DCC", position = 600)
@NbBundle.Messages("CTL_OffsetParameterCalculationActionName=Offset Parameter 계산")
public class OffsetParameterCalculationAction extends AbstractAction {

    @Override
    public void actionPerformed(ActionEvent e) {
        PCSProcessingDialog.createDefaultDialog(new OffsetParameterCalculationOp.Spi(), Bundle.CTL_OffsetParameterCalculationActionName(), SnapApp.getDefault().getAppContext()).show();
    }
}
