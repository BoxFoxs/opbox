package org.satreci.snap.pcs.actions.l1a;

import org.esa.snap.rcp.SnapApp;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;
import org.satreci.snap.pcs.ui.modal.PCSProcessingDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;

@ActionID(category = "GOCI2", id = "org.satreci.snap.soe.l1a.CreateL1AFMProductAction")
@ActionRegistration(displayName = "#CTL_CreateL1AFMProductActionName")
@ActionReference(path = "Menu/GOCI-II/L1A", position = 600)
@NbBundle.Messages("CTL_CreateL1AFMProductActionName=L1A 영상 생성(Forward)")
public class CreateL1AProductActionFM extends AbstractAction {

    @Override
    public void actionPerformed(ActionEvent e) {
        PCSProcessingDialog.createDefaultDialog(new CreateL1AProductOp.FMSpi(), Bundle.CTL_CreateL1AFMProductActionName(), SnapApp.getDefault().getAppContext()).show();
    }
}
