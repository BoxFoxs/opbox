package org.satreci.snap.pcs.actions.lv0;

import com.bc.ceres.core.ProgressMonitor;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.Operator;
import org.esa.snap.core.gpf.OperatorException;
import org.esa.snap.core.gpf.annotations.OperatorMetadata;
import org.esa.snap.core.gpf.annotations.SourceProduct;
import org.satreci.snap.pcs.preferences.PCSOptionsUtil;
import org.satreci.snap.pcs.py.ProcessWrapper;
import org.satreci.snap.pcs.py.PyInterface;
import org.satreci.snap.pcs.ui.InputFileChooser;
import org.satreci.snap.pcs.ui.modal.operator.annotation.TargetProduct;
import org.satreci.snap.pcs.ui.modal.operator.GOCIOperatorSpi;

import java.io.File;

@SuppressWarnings({"UnusedDeclaration", "MismatchedReadAndWriteOfArray, FieldCanBeLocal"})
@OperatorMetadata(alias = "L0AncDecOp",
        category = "GOCI-II",
        version = "0.0.1",
        authors = "Satreci",
        copyright = "",
        description = "GOCI-II Ancillary Decomposition Processing")
public class AncillaryDecompositionOp extends Operator implements ProcessWrapper.FinishListener {

    @SourceProduct(alias = "source", label = "RAW Data", description = "The source product.")
    private Product sourceProduct;

    @TargetProduct(label = "CSV File")
    private Product targetProduct;

    @Override
    public void initialize() throws OperatorException {

    }

    @Override
    public void doExecute(ProgressMonitor pm) throws OperatorException {
        PyInterface.createProcessBuilder("L0AncillaryMain", sourceProduct.getFileLocation()).startBlocking(Bundle.CTL_AncillaryDecompositionActionName(), this);
    }

    @Override
    public void end(String[] outputs) {
        if (outputs.length > 0) {
            File csv = new File(PCSOptionsUtil.getWorkingDir() + outputs[0]);
            targetProduct = new Product(csv.getName(), "csv");
            targetProduct.setFileLocation(csv);
        }
    }

    @Override
    public void fail(String msg) {
        InputFileChooser.showErrorDialog(msg, Bundle.CTL_AncillaryDecompositionActionName());
    }

    public static class Spi extends GOCIOperatorSpi {

        public Spi() {
            super(AncillaryDecompositionOp.class);
        }

    }
}
