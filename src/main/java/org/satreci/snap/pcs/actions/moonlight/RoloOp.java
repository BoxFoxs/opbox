package org.satreci.snap.pcs.actions.moonlight;

import com.bc.ceres.core.ProgressMonitor;
import org.esa.snap.core.dataio.ProductIO;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.datamodel.ProductData;
import org.esa.snap.core.gpf.Operator;
import org.esa.snap.core.gpf.OperatorException;
import org.esa.snap.core.gpf.annotations.OperatorMetadata;
import org.esa.snap.core.gpf.annotations.Parameter;
import org.esa.snap.core.gpf.annotations.SourceProduct;
import org.esa.snap.core.gpf.annotations.TargetProduct;
import org.esa.snap.rcp.SnapApp;
import org.satreci.snap.dataio.bin.BinProductReader;
import org.satreci.snap.pcs.preferences.PCSOptionsUtil;
import org.satreci.snap.pcs.py.ProcessWrapper;
import org.satreci.snap.pcs.py.PyInterface;
import org.satreci.snap.pcs.ui.InputFileChooser;
import org.satreci.snap.pcs.ui.modal.operator.GOCIOperatorSpi;

import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;

@SuppressWarnings({"UnusedDeclaration", "MismatchedReadAndWriteOfArray, FieldCanBeLocal"})
@OperatorMetadata(alias = "RoloOp",
        category = "GOCI-II",
        version = "0.0.1",
        authors = "Satreci",
        copyright = "",
        description = "GOCI-II ROLO Processing")
public class RoloOp extends Operator implements ProcessWrapper.FinishListener {

    @Parameter(description = "DN Threshold", label = "DN Threshold", defaultValue = "0")
    private Integer DNThreshold = 0;

    @Parameter(description = "upper left x position of moon area", label = "Upper Left X", defaultValue = "0")
    private Integer upperLeftX = 0;

    @Parameter(description = "upper left y position of moon area", label = "Upper Left Y", defaultValue = "0")
    private Integer upperLeftY = 0;

    @Parameter(description = "lower right x position of moon area", label = "Lower Right X", defaultValue = "0")
    private Integer lowerRightX = 0;

    @Parameter(description = "lower right y position of moon area", label = "Lower Right Y", defaultValue = "0")
    private Integer lowerRightY = 0;

    @SourceProduct(alias = "source", label = "FDS Data", description = "The source product.")
    private Product fdsData;

    @SourceProduct(alias = "source", label = "달 영상", description = "The source product.")
    private Product moonRaster;

    @SourceProduct(alias = "source", label = "달 영상 헤더", description = "The source product.")
    private Product moonRasterHdr;

    @SourceProduct(alias = "source", label = "Header", description = "The source product.")
    private Product header;

    @TargetProduct(label = "Temp product")
    private Product targetProduct;

    @Override
    public void initialize() throws OperatorException {
    }

    private ProgressMonitor pm;

    @Override
    public void doExecute(ProgressMonitor pm) throws OperatorException {
        File fdsDataFile = fdsData.getFileLocation();
        File moonPxl = moonRaster.getFileLocation();
        File moonHdr = moonRasterHdr.getFileLocation();
        File headerFile = header.getFileLocation();
        PyInterface.createProcessBuilder("roloMain", fdsData, moonPxl, moonHdr, header, DNThreshold, upperLeftX, upperLeftY, lowerRightX, lowerRightY).start(Bundle.CTL_MoonLightVerificationActionName(), this);
    }

    @Override
    public void end(String[] outputs) {
        BinProductReader reader = ((BinProductReader) ProductIO.getProductReader("Binary product"));
        reader.initBandReader(lowerRightY - upperLeftY, lowerRightX - upperLeftX, Double.SIZE, ProductData.TYPE_FLOAT64, 1, ByteOrder.LITTLE_ENDIAN);
        try {
            targetProduct = reader.readProductNodes(new File(PCSOptionsUtil.getWorkingDir() + outputs[0]), null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[][] tableData = new String[outputs.length][2];
        int count = 0;
        tableData[count] = new String[]{"moonIrradiance", outputs[++count]};
        tableData[count] = new String[]{"countMoonPixels", outputs[++count]};
        tableData[count] = new String[]{"sumMoonPixels", outputs[++count]};
        tableData[count] = new String[]{"LunarAgingFactor", outputs[++count]};
        tableData[count] = new String[]{"SlopeFactor", outputs[++count]};
        new ResultTableDialog(SnapApp.getDefault().getMainFrame(), tableData).setVisible(true);
    }

    @Override
    public void fail(String msg) {
        InputFileChooser.showErrorDialog(msg, Bundle.CTL_MoonLightVerificationActionName());
    }

    public static class Spi extends GOCIOperatorSpi {

        public Spi() {
            super(RoloOp.class);
        }

    }


}
