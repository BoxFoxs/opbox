package org.satreci.snap.pcs.actions.moonlight;

import javax.swing.*;
import java.awt.*;

public class ResultTableDialog extends JDialog {

    public ResultTableDialog(Frame parent, String[][] tableData) {
        super(parent);
        initComponents(tableData);
    }

    private void initComponents(String[][] tableData) {
        JTable table = new JTable(tableData, new String[]{"Name", "Value"});
        table.setDefaultEditor(Object.class, null);
        JScrollPane scrollPane = new JScrollPane(table);

        this.add(scrollPane);
        this.setModal(false);
        this.pack();
        this.setLocationRelativeTo(getParent());
    }
}
