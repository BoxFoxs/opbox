package org.satreci.snap.pcs.actions.dcc;

import com.bc.ceres.core.ProgressMonitor;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.Operator;
import org.esa.snap.core.gpf.OperatorException;
import org.esa.snap.core.gpf.annotations.OperatorMetadata;
import org.esa.snap.core.gpf.annotations.SourceProduct;
import org.satreci.snap.dataio.bin.ProductSize;
import org.satreci.snap.dataio.pcs.PCSProductReader;
import org.satreci.snap.pcs.preferences.PCSOptionsUtil;
import org.satreci.snap.pcs.py.ProcessWrapper;
import org.satreci.snap.pcs.py.PyInterface;
import org.satreci.snap.pcs.ui.InputFileChooser;
import org.satreci.snap.pcs.ui.modal.operator.annotation.TargetProduct;
import org.satreci.snap.pcs.ui.modal.operator.GOCIOperatorSpi;

import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;

@SuppressWarnings({"UnusedDeclaration", "MismatchedReadAndWriteOfArray, FieldCanBeLocal"})
@OperatorMetadata(alias = "DCCOffsetParamOp",
        category = "GOCI-II",
        version = "0.0.1",
        authors = "Satreci",
        copyright = "",
        description = "GOCI-II Offset Parameter Calculation Processing")
public class OffsetParameterCalculationOp extends Operator implements ProcessWrapper.FinishListener {

    @SourceProduct(alias = "dark", type = TargetProduct.DIR, label = "GOCI-II Dark Location", description = "GOCI-II Dark Location")
    private Product dark;

    @TargetProduct(label = "Result")
    private Product targetProduct;

    @Override
    public void initialize() throws OperatorException {

    }

    @Override
    public void doExecute(ProgressMonitor pm) throws OperatorException {
        PyInterface.createProcessBuilder("dccOffsetParameterMain", dark.getFileLocation()).startBlocking(Bundle.CTL_LinearityCalculationActionName(), this);
    }

    @Override
    public void end(String[] outputs) {
        try {
            PCSProductReader reader = new PCSProductReader("DCC Offset Parameter Calculation", 2720, 2720, ProductSize.FLOAT, ByteOrder.LITTLE_ENDIAN);
            reader.addBand(new File(PCSOptionsUtil.getWorkingDir() + outputs[0]));
            reader.addBand(new File(PCSOptionsUtil.getWorkingDir() + outputs[1]));
            targetProduct = reader.readProductNodes(null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fail(String msg) {
        InputFileChooser.showErrorDialog(msg, Bundle.CTL_OffsetParameterCalculationActionName());
    }

    public static class Spi extends GOCIOperatorSpi {

        public Spi() {
            super(OffsetParameterCalculationOp.class);
        }

    }
}