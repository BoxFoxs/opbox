package org.satreci.snap.pcs.actions.l1a;

import com.bc.ceres.core.ProgressMonitor;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.Operator;
import org.esa.snap.core.gpf.OperatorException;
import org.esa.snap.core.gpf.annotations.OperatorMetadata;
import org.esa.snap.core.gpf.annotations.SourceProduct;
import org.satreci.snap.dataio.bin.ProductSize;
import org.satreci.snap.dataio.pcs.PCSProductReader;
import org.satreci.snap.pcs.preferences.PCSOptionsUtil;
import org.satreci.snap.pcs.py.ProcessWrapper;
import org.satreci.snap.pcs.py.PyInterface;
import org.satreci.snap.pcs.ui.InputFileChooser;
import org.satreci.snap.pcs.ui.modal.operator.annotation.TargetProduct;
import org.satreci.snap.pcs.ui.modal.operator.GOCIOperatorSpi;
import org.satreci.snap.pcs.util.InputVerification;

import java.io.File;
import java.io.IOException;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings({"UnusedDeclaration", "MismatchedReadAndWriteOfArray, FieldCanBeLocal"})
@OperatorMetadata(alias = "L0VideoPixelOp",
        category = "GOCI-II",
        version = "0.0.1",
        authors = "Satreci",
        copyright = "",
        description = "GOCI-II Video Pixel Decomposition Processing")
public class CreateL1AProductOp extends Operator implements ProcessWrapper.FinishListener {

    @SourceProduct(alias = "HG", type = TargetProduct.BAND, optional = true, label = "HG", description = "HG Image")
    private Product hgImage;

    @SourceProduct(alias = "LG", type = TargetProduct.BAND, optional = true, label = "LG", description = "LG Image")
    private Product lgImage;

    @SourceProduct(alias = "Offset", label = "Offset", description = "Offset File")
    private Product offset;

    @SourceProduct(alias = "fixedOffset", label = "Fixed Offset", description = "Fixed Offset File")
    private Product fixedOffset;

    @SourceProduct(alias = "LinearGainMatrix", label = "Linear Gain Matrix", description = "Linear Gain Matrix File")
    private Product linearGainMatrix;

    @SourceProduct(alias = "nonlinearGainA", label = "nonlinear Gain A", description = "nonlinear Gain A File")
    private Product nonlinearGainA;

    @SourceProduct(alias = "nonlinearGainB", label = "nonlinear Gain B", description = "nonlinear Gain B File")
    private Product nonlinearGainB;

    @TargetProduct(label = "Result")
    private Product targetProduct;

    @Override
    public void initialize() throws OperatorException {

    }

    private static File productToFile(Product product) {
        File file = null;
        if (product != null)
            file = product.getFileLocation();
        return file;
    }

    @Override
    public void doExecute(ProgressMonitor pm) throws OperatorException {
        if (!InputVerification.checkNotNull(hgImage, lgImage, offset, fixedOffset, linearGainMatrix, nonlinearGainA, nonlinearGainB)) {
            if (hgImage == null && lgImage == null) {
                InputFileChooser.showErrorDialog("Invalid input file", "Generate L1 Processing");
                throw new OperatorException("Need to Select HG or LG Data");
            }
        }
        String type = (this.getSpi() instanceof IMSpi) ? "IM" : "FM";
        List argments = Arrays.asList(type, productToFile(hgImage), productToFile(lgImage), productToFile(offset), productToFile(fixedOffset), productToFile(linearGainMatrix), productToFile(nonlinearGainA), productToFile(nonlinearGainB));
        PyInterface.createProcessBuilder("L1aOffsetParameterMain", argments.toArray()).startBlocking(Bundle.CTL_OffsetParameterCalculationActionName(), this);
    }

    @Override
    public void end(String[] outputs) {
        try {
            PCSProductReader reader = new PCSProductReader("L1 Product", 2720, 2718, ProductSize.DOUBLE, ByteOrder.LITTLE_ENDIAN);
            for (String output : outputs) {
                if (output.length() > 0)
                    reader.addBand(new File(PCSOptionsUtil.getWorkingDir() + outputs));
            }
            targetProduct = reader.readProductNodes(null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void fail(String msg) {
        InputFileChooser.showErrorDialog(msg, Bundle.CTL_OffsetParameterCalculationActionName());
    }

    public static class FMSpi extends GOCIOperatorSpi {

        public FMSpi() {
            super(CreateL1AProductOp.class);
        }

    }

    public static class IMSpi extends GOCIOperatorSpi {

        public IMSpi() {
            super(CreateL1AProductOp.class);
        }

    }
}