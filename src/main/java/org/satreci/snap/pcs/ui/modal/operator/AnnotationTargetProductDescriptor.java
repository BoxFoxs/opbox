package org.satreci.snap.pcs.ui.modal.operator;

import com.bc.ceres.core.Assert;
import org.esa.snap.core.datamodel.Product;
import org.satreci.snap.pcs.ui.modal.operator.annotation.TargetProduct;
import org.esa.snap.core.gpf.descriptor.TargetProductDescriptor;

import java.io.File;

public class AnnotationTargetProductDescriptor implements TargetProductDescriptor {
    private final String name;
    private final TargetProduct annotation;

    public AnnotationTargetProductDescriptor(String name, TargetProduct annotation) {
        Assert.notNull(name, "name");
        Assert.notNull(annotation, "annotation");
        this.name = name;
        this.annotation = annotation;
    }

    public String getName() {
        return this.name;
    }

    public TargetProduct getAnnotation() {
        return this.annotation;
    }

    public String getAlias() {
        return this.annotation.alias();
    }

    public String getLabel() {
        return getNonEmptyStringOrNull(this.annotation.label());
    }

    public String getDescription() {
        return getNonEmptyStringOrNull(this.annotation.description());
    }

    public Class<? extends Product> getDataType() {
        return null;
    }

    public Class getCustomDataType() {
        if (this.annotation.type().equals(TargetProduct.PRODUCT))
            return Product.class;
        else return File.class;
    }

    private static String getNonEmptyStringOrNull(String label) {
        return label != null && !label.isEmpty() ? label : null;
    }
}