package org.satreci.snap.pcs.ui.modal.operator;

import org.esa.snap.core.dataio.ProductIO;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.Operator;
import org.esa.snap.core.gpf.OperatorSpi;
import org.esa.snap.core.gpf.annotations.Parameter;
import org.esa.snap.core.gpf.annotations.SourceProduct;
import org.esa.snap.core.gpf.descriptor.TargetProductDescriptor;
import org.esa.snap.core.gpf.ui.TargetProductSelectorModel;
import org.satreci.snap.pcs.util.FileUtil;
import org.satreci.snap.pcs.ui.modal.operator.annotation.TargetProduct;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class GOCIOperatorSpi extends OperatorSpi {

    protected GOCIOperatorSpi(Class<? extends Operator> operatorClass) {
        super(operatorClass);
    }

    @Override
    public Operator createOperator(Map<String, Object> parameters, Map<String, Product> sourceProducts) {
        try {
            Operator operator = getOperatorClass().newInstance();
            fieldStream(operator.getClass()).forEach(field -> {
                annotationStream(field).forEach(annotation -> {
                    if (annotation instanceof SourceProduct) {
                        SourceProduct sourceProduct = (SourceProduct) annotation;
                        try {
                            field.setAccessible(true);
                            field.set(operator, sourceProducts.get(sourceProduct.alias()));
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    } else if (annotation instanceof Parameter) {
                        Parameter parameter = (Parameter) annotation;
                        try {
                            field.setAccessible(true);
                            field.set(operator, parameters.get(parameter.alias()));
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                });
            });
            return operator;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Stream<Field> fieldStream(Class classz) {
        Field[] fields = classz.getDeclaredFields();
        return Arrays.stream(fields);
    }

    private static Stream<Annotation> annotationStream(Field field) {
        return Arrays.stream(field.getDeclaredAnnotations());
    }


    public static Product[] getTargetProducts(Operator operator, Map<String, TargetProductSelectorModel> targetProductSelectorModelMap) {
        List<Product> targetProducts = new ArrayList<>();
        fieldStream(operator.getClass()).forEach(field -> {
            annotationStream(field).forEach(annotation -> {
                if (annotation instanceof TargetProduct) {
                    try {
                        TargetProduct targetProductAnnotation = (TargetProduct) annotation;
                        TargetProductSelectorModel targetProductSelectorModel = targetProductSelectorModelMap.get(targetProductAnnotation.alias());

                        if (targetProductSelectorModel.isSaveToFileSelected()) {
                            field.setAccessible(true);
                            if (targetProductAnnotation.type().equals(TargetProduct.PRODUCT)) {
                                Product product = (Product) field.get(operator);
                                ProductIO.getProductWriter(targetProductAnnotation.writer()).writeProductNodes(product, targetProductSelectorModel.getProductFile());
                            } else {
                                Product product = (Product) field.get(operator);
                                FileUtil.copyFile(product.getFileLocation(), new File(FileUtil.replaceExtension(targetProductSelectorModel.getProductFile().getPath(), targetProductAnnotation.ext())));
                                FileUtil.deleteFile(product.getFileLocation());
                            }
                        }
                        if (targetProductAnnotation.type().equals(TargetProduct.PRODUCT) && targetProductSelectorModel.isOpenInAppSelected()) {
                            field.setAccessible(true);
                            Product product = (Product) field.get(operator);
                            product.setName(targetProductSelectorModel.getProductName());
                            targetProducts.add(product);
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        });
        return targetProducts.toArray(new Product[targetProducts.size()]);
    }

    public static void fileCopy(String inFileName, String outFileName) {
        try {
            FileInputStream fis = new FileInputStream(inFileName);
            FileOutputStream fos = new FileOutputStream(outFileName);

            int data = 0;
            while ((data = fis.read()) != -1) {
                fos.write(data);
            }
            fis.close();
            fos.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public List<TargetProductDescriptor> getTargetProductsDescriptor() {
        List<TargetProductDescriptor> targetProductDescriptors = new ArrayList<>();
        fieldStream(getOperatorClass()).forEach(field -> {
            annotationStream(field).forEach(annotation -> {
                if (annotation instanceof TargetProduct) {
                    TargetProduct targetProductAnnotation = (TargetProduct) annotation;
                    targetProductDescriptors.add(new AnnotationTargetProductDescriptor(targetProductAnnotation.label(), targetProductAnnotation));
                }
            });
        });
        return targetProductDescriptors;
    }

}
