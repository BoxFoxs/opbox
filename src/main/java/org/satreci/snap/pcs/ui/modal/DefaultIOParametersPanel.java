package org.satreci.snap.pcs.ui.modal;

import com.bc.ceres.binding.PropertyDescriptor;
import com.bc.ceres.swing.TableLayout;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.datamodel.ProductFilter;
import org.esa.snap.core.gpf.OperatorSpi;
import org.esa.snap.core.gpf.descriptor.OperatorDescriptor;
import org.esa.snap.core.gpf.descriptor.SourceProductDescriptor;
import org.esa.snap.core.gpf.descriptor.TargetProductDescriptor;
import org.esa.snap.core.gpf.ui.TargetProductSelectorModel;
import org.esa.snap.ui.AppContext;
import org.satreci.snap.pcs.ui.modal.operator.annotation.TargetProduct;
import org.satreci.snap.pcs.ui.modal.operator.AnnotationTargetProductDescriptor;
import org.satreci.snap.pcs.ui.modal.operator.GOCIOperatorSpi;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * WARNING: This class belongs to a preliminary API and may change in future releases.
 */
public class DefaultIOParametersPanel extends JPanel {

    private ArrayList<SourceProductSelector> sourceProductSelectorList;
    private ArrayList<TargetProductSelector> targetProductSelectorList;
    private HashMap<TargetProductDescriptor, TargetProductSelector> targetProductSelectorMap;
    private HashMap<SourceProductDescriptor, SourceProductSelector> sourceProductSelectorMap;
    private AppContext appContext;

    public DefaultIOParametersPanel(AppContext appContext, OperatorDescriptor descriptor, List<TargetProductDescriptor> targetProductDescriptors) {
        this.appContext = appContext;
        sourceProductSelectorList = new ArrayList<>(3);
        targetProductSelectorList = new ArrayList<>(3);
        sourceProductSelectorMap = new HashMap<>(3);
        targetProductSelectorMap = new HashMap<>(3);
        // Fetch source products
        createSourceProductSelectors(descriptor);
        createTargetProductSelectors(targetProductDescriptors);
        if (!sourceProductSelectorList.isEmpty()) {
            setSourceProductSelectorLabels();
            setSourceProductSelectorToolTipTexts();
        }
        if (!targetProductSelectorList.isEmpty()) {
            setTargetProductSelectorLabels();
            setTargetProductSelectorToolTipTexts();
        }

        final TableLayout tableLayout = new TableLayout(1);
        tableLayout.setTableAnchor(TableLayout.Anchor.WEST);
        tableLayout.setTableWeightX(1.0);
        tableLayout.setTableFill(TableLayout.Fill.HORIZONTAL);
        tableLayout.setTablePadding(3, 3);

        setLayout(tableLayout);
        int countSPS = sourceProductSelectorList.size();
        if (countSPS == 1) {
            for (SourceProductSelector selector : sourceProductSelectorList) {
                add(selector.createDefaultPanel());
            }
        } else {
            final TableLayout tableLayoutSPS = new TableLayout(1);
            tableLayoutSPS.setTableAnchor(TableLayout.Anchor.WEST);
            tableLayoutSPS.setTableWeightX(1.0);
            tableLayoutSPS.setTableFill(TableLayout.Fill.HORIZONTAL);
            JPanel panel = new JPanel(tableLayoutSPS);
            panel.setBorder(BorderFactory.createTitledBorder("Source Products"));
            for (SourceProductSelector selector : sourceProductSelectorList) {
                panel.add(selector.createDefaultPanel(""));
            }
            add(panel);
        }

        final TableLayout tableLayoutSPS = new TableLayout(1);
        JPanel panel = new JPanel(tableLayoutSPS);
        panel.setBorder(BorderFactory.createTitledBorder("Result Products(Files)"));
        for (TargetProductSelector selector : targetProductSelectorList) {
            panel.add(selector.createDefaultPanel());
        }
        add(panel);
        add(tableLayout.createVerticalSpacer());
    }

    @Deprecated
    public DefaultIOParametersPanel(AppContext appContext, OperatorSpi operatorSpi) {
        this(appContext, operatorSpi.getOperatorDescriptor(), ((GOCIOperatorSpi) operatorSpi).getTargetProductsDescriptor());
    }

    public List<TargetProductSelector> getTargetProductSelectorList() {
        return targetProductSelectorList;
    }

    public ArrayList<SourceProductSelector> getSourceProductSelectorList() {
        return sourceProductSelectorList;
    }

    public void initSourceProductSelectors() {
        for (SourceProductSelector sourceProductSelector : sourceProductSelectorList) {
            sourceProductSelector.initProducts();
        }
    }

    public void releaseSourceProductSelectors() {
        for (SourceProductSelector sourceProductSelector : sourceProductSelectorList) {
            sourceProductSelector.releaseProducts();
        }
    }

    public HashMap<String, Product> createSourceProductsMap() {
        final HashMap<String, Product> sourceProducts = new HashMap<>(8);
        for (SourceProductDescriptor descriptor : sourceProductSelectorMap.keySet()) {
            final SourceProductSelector selector = sourceProductSelectorMap.get(descriptor);
            String alias = descriptor.getAlias();
            String key = alias != null ? alias : descriptor.getName();
            sourceProducts.put(key, selector.getSelectedProduct());
        }
        return sourceProducts;
    }

    public HashMap<String, TargetProductSelectorModel> createTargetProductSelectorModelMap() {
        final HashMap<String, TargetProductSelectorModel> sourceProducts = new HashMap<>(8);
        for (TargetProductDescriptor descriptor : targetProductSelectorMap.keySet()) {
            final TargetProductSelector selector = targetProductSelectorMap.get(descriptor);
            String alias = descriptor.getAlias();
            String key = alias != null ? alias : descriptor.getName();
            sourceProducts.put(key, selector.getModel());
        }
        return sourceProducts;
    }


    private void createSourceProductSelectors(OperatorDescriptor operatorDescriptor) {
        for (SourceProductDescriptor descriptor : operatorDescriptor.getSourceProductDescriptors()) {
            SourceProductSelector sourceProductSelector = new SourceProductSelector(appContext, descriptor.isOptional());
            sourceProductSelector.setTargetMode(descriptor.getProductType());
            sourceProductSelectorList.add(sourceProductSelector);
            sourceProductSelectorMap.put(descriptor, sourceProductSelector);
        }
    }

    private void createTargetProductSelectors(List<TargetProductDescriptor> targetProductDescriptors) {
        for (TargetProductDescriptor descriptor : targetProductDescriptors) {
            TargetProductSelector targetProductSelector = new TargetProductSelector();
            targetProductSelector.setType(((AnnotationTargetProductDescriptor) descriptor).getCustomDataType());
            targetProductSelectorList.add(targetProductSelector);
            targetProductSelectorMap.put(descriptor, targetProductSelector);
        }
    }

    private void setTargetProductSelectorLabels() {
        for (TargetProductDescriptor descriptor : targetProductSelectorMap.keySet()) {
            final TargetProductSelector selector = targetProductSelectorMap.get(descriptor);
            if (selector != null) {
                String label = descriptor.getLabel();
                String alias = descriptor.getAlias();
                if (label == null && alias != null) {
                    label = alias;
                }
                if (label == null) {
                    label = PropertyDescriptor.createDisplayName(descriptor.getName());
                }
                if (!label.endsWith(":")) {
                    label += ":";
                }
                selector.getProductNameLabel().setText(label);
            }
        }
    }

    private void setSourceProductSelectorLabels() {
        for (SourceProductDescriptor descriptor : sourceProductSelectorMap.keySet()) {
            final SourceProductSelector selector = sourceProductSelectorMap.get(descriptor);
            String label = descriptor.getLabel();
            String alias = descriptor.getAlias();
            if (label == null && alias != null) {
                label = alias;
            }
            if (label == null) {
                label = PropertyDescriptor.createDisplayName(descriptor.getName());
            }
            if (!label.endsWith(":")) {
                label += ":";
            }
            if (descriptor.isOptional()) {
                label += " (optional)";
            }
            selector.getProductNameLabel().setText(label);
        }
    }

    private void setSourceProductSelectorToolTipTexts() {
        for (SourceProductDescriptor descriptor : sourceProductSelectorMap.keySet()) {
            final String description = descriptor.getDescription();
            if (description != null) {
                final SourceProductSelector selector = sourceProductSelectorMap.get(descriptor);
                selector.getProductNameComboBox().setToolTipText(description);
            }
        }
    }

    private void setTargetProductSelectorToolTipTexts() {
        for (TargetProductDescriptor descriptor : targetProductSelectorMap.keySet()) {
            final String description = descriptor.getDescription();
            if (description != null) {
                final TargetProductSelector selector = targetProductSelectorMap.get(descriptor);
                selector.getProductNameLabel().setToolTipText(description);
            }
        }
    }


    private static class AnnotatedSourceProductFilter implements ProductFilter {

        private final SourceProductDescriptor productDescriptor;

        private AnnotatedSourceProductFilter(SourceProductDescriptor productDescriptor) {
            this.productDescriptor = productDescriptor;
        }

        @Override
        public boolean accept(Product product) {

            String productType = productDescriptor.getProductType();
            if (productType != null && !product.getProductType().matches(productType)) {
                return false;
            }

            for (String bandName : productDescriptor.getBands()) {
                if (!product.containsBand(bandName)) {
                    return false;
                }
            }

            return true;
        }
    }
}