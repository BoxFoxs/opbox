package org.satreci.snap.pcs.ui.modal;

import com.bc.ceres.core.ProgressMonitor;
import com.bc.ceres.swing.progress.ProgressMonitorSwingWorker;
import org.esa.snap.rcp.SnapApp;
import org.esa.snap.rcp.util.Dialogs;
import org.esa.snap.ui.AppContext;
import org.esa.snap.ui.ModelessDialog;

import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.GPF;
import org.esa.snap.core.gpf.OperatorCancelException;
import org.esa.snap.core.gpf.OperatorException;

import javax.swing.AbstractButton;
import java.text.MessageFormat;

public abstract class GOCIProcessingProductDialog extends ModelessDialog {

    protected AppContext appContext;

    protected GOCIProcessingProductDialog(AppContext appContext, String title, String helpID) {
        this(appContext, title, ID_APPLY_CLOSE_HELP, helpID);
    }

    protected GOCIProcessingProductDialog(AppContext appContext, String title, int buttonMask, String helpID) {
        super(appContext.getApplicationWindow(), title, buttonMask, helpID);
        this.appContext = appContext;
        AbstractButton button = getButton(ID_APPLY);
        button.setText("Run");
        button.setMnemonic('R');
    }

    public AppContext getAppContext() {
        return appContext;
    }

    @Override
    protected void onApply() {
        if (!canApply()) {
            return;
        }

        new ProgressMonitorSwingWorker<Void, Void>(this.getParent(), getTitle()) {
            @Override
            protected Void doInBackground(ProgressMonitor progressMonitor) throws Exception {
                progressMonitor.beginTask(getTitle(), 0);
                try {
                    Product[] products = createTargetProduct(progressMonitor);
                    for (Product product : products)
                        SnapApp.getDefault().getProductManager().addProduct(product);
                    GOCIProcessingProductDialog.this.hide();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressMonitor.done();
                return null;
            }
        }.executeWithBlocking();


    }

    protected void handleInitialisationError(Throwable t) {
        String msg;
        if (t instanceof OperatorCancelException) {
            msg = MessageFormat.format("An internal error occurred during the target product initialisation.\n{0}",
                    formatThrowable(t));
            showErrorDialog(msg);

            return;
        }

        if (isInternalException(t)) {
            msg = MessageFormat.format("An internal error occurred during the target product initialisation.\n{0}",
                    formatThrowable(t));
        } else {
            msg = MessageFormat.format("A problem occurred during the target product initialisation.\n{0}",
                    formatThrowable(t));
        }
        appContext.handleError(msg, t);
    }

    protected void handleProcessingError(Throwable t) {
        String msg;

        if (t instanceof OperatorCancelException) {
            return;
        }

        if (isInternalException(t)) {
            msg = MessageFormat.format("An internal error occurred during the target product processing.\n{0}",
                    formatThrowable(t));
        } else {
            msg = MessageFormat.format("A problem occurred during processing the target product processing.\n{0}",
                    formatThrowable(t));
        }
        appContext.handleError(msg, t);
    }

    private boolean isInternalException(Throwable t) {
        return (t instanceof RuntimeException && !(t instanceof OperatorException)) || t instanceof Error;
    }

    private String formatThrowable(Throwable t) {
        return MessageFormat.format("Type: {0}\nMessage: {1}\n",
                t.getClass().getSimpleName(),
                t.getMessage());
    }

    protected abstract boolean canApply();

    protected void showOpenInAppInfo() {
        final String message = MessageFormat.format(
                "<html>The target product has successfully been created and opened in {0}.<br><br>" +
                        "Actual processing of source to target data will be performed only on demand,<br>" +
                        "for example, if the target product is saved or an image view is opened.",
                appContext.getApplicationName()
        );
        showSuppressibleInformationDialog(message, "openInAppInfo");
    }

    private void showSaveAndOpenInAppInfo(long saveTime) {
        final String message = MessageFormat.format(
                "<html>The target product has been successfully written to<br>" +
                        "<p>{0}</p><br>" +
                        "and has been opened in {1}.<br><br>" +
                        "Total time spend for processing: {2}<br>",
                appContext.getApplicationName(),
                formatDuration(saveTime)
        );
        showSuppressibleInformationDialog(message, "saveAndOpenInAppInfo");
    }

    String formatDuration(long millis) {
        long seconds = millis / 1000;
        millis -= seconds * 1000;
        long minutes = seconds / 60;
        seconds -= minutes * 60;
        long hours = minutes / 60;
        minutes -= hours * 60;
        return String.format("%02d:%02d:%02d.%03d", hours, minutes, seconds, millis);
    }

    /**
     * Shows an information dialog on top of this dialog.
     * The shown dialog will contain a user option allowing to not show the message anymore.
     *
     * @param infoMessage  The message.
     * @param propertyName The (simple) property name used to store the user option in the application's preferences.
     */
    public void showSuppressibleInformationDialog(String infoMessage, String propertyName) {
        Dialogs.showInformation(getTitle(), infoMessage, getQualifiedPropertyName(propertyName));
    }

    /**
     * Creates the desired target product.
     * Usually, this method will be implemented by invoking one of the multiple {@link GPF GPF}
     * {@code createProduct} methods.
     * <p>
     * The method should throw a {@link OperatorException} in order to signal "nominal" processing errors,
     * other exeption types are treated as internal errors.
     *
     * @return The target product.
     * @throws Exception if an error occurs, an {@link OperatorException} is signaling "nominal" processing errors.
     */
    protected abstract Product[] createTargetProduct(ProgressMonitor pm) throws Exception;
}