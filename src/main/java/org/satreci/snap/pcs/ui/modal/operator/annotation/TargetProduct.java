package org.satreci.snap.pcs.ui.modal.operator.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface TargetProduct {
    String PRODUCT = "PRODUCT";
    String FILE = "FILE";
    String BAND = "BAND";
    String DIR = "DIR";

    String label() default "";

    String type() default PRODUCT;

    String description() default "";

    String alias() default "Target";

    String ext() default "pxl";

    String writer() default "PCS Product";
}