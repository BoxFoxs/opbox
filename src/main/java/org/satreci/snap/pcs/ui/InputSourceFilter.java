package org.satreci.snap.pcs.ui;

import java.io.File;

public interface InputSourceFilter {

    boolean accept(File file);
}
