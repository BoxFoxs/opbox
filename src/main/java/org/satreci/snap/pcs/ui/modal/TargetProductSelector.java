package org.satreci.snap.pcs.ui.modal;

import com.bc.ceres.swing.TableLayout;
import org.esa.snap.core.datamodel.Product;

import javax.swing.*;
import java.awt.*;

public class TargetProductSelector extends org.esa.snap.core.gpf.ui.TargetProductSelector {
    private boolean canLookupOutput = true;

    @Override
    public JPanel createDefaultPanel() {
        final JPanel subPanel1 = new JPanel(new BorderLayout(3, 3));
        subPanel1.add(getProductNameLabel(), BorderLayout.NORTH);
        subPanel1.add(getProductNameTextField(), BorderLayout.CENTER);

        JPanel subPanel2 = new JPanel(new FlowLayout(FlowLayout.LEADING, 0, 0));
        subPanel2.add(getSaveToFileCheckBox());
        subPanel2.add(getProductDirTextField(), BorderLayout.CENTER);
        subPanel2.add(getProductDirChooserButton(), BorderLayout.EAST);

        final TableLayout tableLayout = new TableLayout(1);
        tableLayout.setTableAnchor(TableLayout.Anchor.WEST);
        tableLayout.setTableFill(TableLayout.Fill.HORIZONTAL);
        tableLayout.setTableWeightX(1.0);

        tableLayout.setCellPadding(0, 0, new Insets(3, 3, 3, 3));
        tableLayout.setCellPadding(2, 0, new Insets(0, 3, 3, 3));
        tableLayout.setCellPadding(3, 0, new Insets(3, 3, 3, 3));

        final JPanel panel = new JPanel(tableLayout);
        panel.add(subPanel1);

        panel.add(subPanel2);

        if (canLookupOutput)
            panel.add(getOpenInAppCheckBox());

        return panel;
    }

    public void setType(Class classz) {
        if (classz != Product.class) {
            this.canLookupOutput = false;
        }
    }
}
