package org.satreci.snap.pcs.ui;

import com.bc.ceres.swing.TableLayout;
import org.esa.snap.rcp.SnapApp;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputFileChooser extends JDialog {
    public enum InputType {NUMBER}

    private static JFileChooser chooser = new JFileChooser();

    private JPanel stackLayout, mainContent;
    private Map<String, InputForm> pathMap;
    private List<String> keySet;
    private SubmitListener listener;

    public InputFileChooser(Frame parent, String title, SubmitListener listener) {
        super(parent, title);
        this.listener = listener;
        pathMap = new HashMap<>();
        keySet = new ArrayList<>();
        stackLayout = new JPanel(new StackLayout());
        JPanel buttonContent = new JPanel();
        JPanel buttonsWrapper = new JPanel();
        buttonContent.setLayout(new BorderLayout());
        JButton submitBtn = new JButton("확인");
        JButton cancelBtn = new JButton("취소");
        submitBtn.addActionListener((e) -> onSubmit());
        cancelBtn.addActionListener((e) -> onCancel());
        buttonsWrapper.add(submitBtn);
        buttonsWrapper.add(cancelBtn);
        buttonContent.add(buttonsWrapper, BorderLayout.EAST);
        buttonContent.setBorder(new EmptyBorder(10, 0, 0, 0));
        this.add(buttonContent, BorderLayout.SOUTH);
        this.add(stackLayout, BorderLayout.CENTER);
    }

    private void initMainContent() {
        TableLayout tableLayout = new TableLayout(3);
        tableLayout.setTableAnchor(TableLayout.Anchor.NORTHWEST);
        tableLayout.setTablePadding(new Insets(4, 10, 0, 0));
        tableLayout.setTableFill(TableLayout.Fill.BOTH);
        tableLayout.setColumnWeightX(1, 2.0);
        mainContent = new JPanel(tableLayout);
        mainContent.setBorder(new EmptyBorder(0, 10, 0, 15));
        stackLayout.add(mainContent);
    }

    private void onSubmit() {
        this.setVisible(false);
        for (String key : keySet) {
            File file = (pathMap.get(key).getPath().length() == 0) ? null : new File(pathMap.get(key).getPath());
            if (file == null && pathMap.get(key).isEnabled()) {
                showErrorDialog(NbBundle.getMessage(InputFileChooser.class,
                        "SOE.guideErrorAllInputChoose.text"), this.getTitle());
                return;
            }
        }
        listener.onSubmit(getResults());
    }

    private void onCancel() {
        this.setVisible(false);
    }

    public void setVisible(boolean check) {
        if (check) {
            this.pack();
            this.setLocationRelativeTo(SnapApp.getDefault().getMainFrame());
        }
        super.setVisible(check);
    }

    public File[] getResults() {
        Stream stream = keySet.stream().map(key -> (pathMap.get(key).getPath().length() == 0) ? null : new File(pathMap.get(key).getPath()));
        return ((List<File>) stream.collect(Collectors.toList())).toArray(new File[keySet.size()]);
    }

    public void addCustomView(JPanel panel) {
        stackLayout.add(panel);
    }

    public void setEnable(String str, boolean check) {
        pathMap.get(str).setEnabled(check);
    }

    public void addInputItem(String name, boolean type) {
        this.addInputItem(name, type, null);
    }

    public void addInputItem(String name, boolean type, InputValidationFilter filter) {
        if (mainContent == null)
            initMainContent();
        JTextField pathField = new JTextField();
        Dimension preferredSize = pathField.getPreferredSize();
        preferredSize.width = 150;
        pathField.setPreferredSize(preferredSize);
        pathField.setEditable(false);
        JButton findButton = new JButton("find");
        findButton.addActionListener((event) -> {
            File file = showChooser(name, type, this);
            if (file != null && ((filter != null && filter.isValid(file)) || filter == null)) {
                pathField.setText(file.getPath());
            } else if (file != null && filter != null) {
                showErrorDialog(NbBundle.getMessage(InputFileChooser.class, "SOE.guideErrorInputTypeInvalid.text"), this.getTitle());
            }
        });
        JLabel label = new JLabel(name);
        mainContent.add(label);
        mainContent.add(pathField);
        mainContent.add(findButton);
        pathMap.put(name, new InputForm(label, pathField, findButton));
        keySet.add(name);
    }

    public void addInputField(String name, InputType inputType) {
        mainContent.add(new JLabel(name));
        JTextField field = new JTextField();
        if (inputType == InputType.NUMBER) {
            field = new JFormattedTextField(NumberFormat.getNumberInstance());
            field.setText("0");
        }
        mainContent.add(field);
        mainContent.add(new JPanel());
    }

    public static File showChooser(String title, boolean type, Component parent) {
        chooser.setSelectedFile(null);
        chooser.setDialogTitle(title);
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        if (!type) chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (chooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
            return chooser.getSelectedFile();
        }
        return null;
    }

    public static void showErrorDialog(String msg, String title) {
        JOptionPane.showMessageDialog(SnapApp.getDefault().getMainFrame(), msg, title, JOptionPane.ERROR_MESSAGE);
    }

    public interface InputValidationFilter {
        boolean isValid(File file);
    }

    public interface SubmitListener {
        void onSubmit(Object[] results);
    }

    private class InputForm {
        private JTextField pathField;
        private JLabel label;
        private JButton searchBtn;

        public InputForm(JLabel label, JTextField field, JButton btn) {
            this.label = label;
            this.pathField = field;
            this.searchBtn = btn;
        }

        public String getPath() {
            return pathField.getText();
        }

        public void setEnabled(boolean check) {
            searchBtn.setEnabled(check);
            if (!check)
                pathField.setText("");
        }

        public boolean isEnabled() {
            return searchBtn.isEnabled();
        }
    }
}
