package org.satreci.snap.pcs.py;

import com.bc.ceres.core.ProgressMonitor;
import org.esa.snap.rcp.SnapApp;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ProcessWrapper {
    private ProcessBuilder pb;
    private Process process;

    public ProcessWrapper(ProcessBuilder pb) {
        this.pb = pb;
    }

    public boolean isRunning() {
        return (process != null && process.isAlive());
    }

    public void startBlocking(String actionName, FinishListener listener) {
        Object result = executeProcess();
        processResult(listener, result);
    }

    public void start(String actionName, FinishListener listener) {
        ProgressDialog progressDialog = new ProgressDialog(SnapApp.getDefault().getMainFrame(), actionName, "");
        new SwingWorker<Object, Object>() {
            @Override
            protected Object doInBackground() throws Exception {
                progressDialog.setVisible(true);
                return executeProcess();
            }

            @Override
            protected void done() {
                progressDialog.setVisible(false);
                try {
                    processResult(listener, get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    private void processResult(FinishListener listener, Object result) {
        if (result instanceof String[])
            listener.end((String[]) result);
        else listener.fail((String) result);
    }

    private Object executeProcess() {
        try {
            process = pb.start();
            StdWorker errStream = new StdWorker(process.getErrorStream());
            errStream.execute();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            List<String> outputs = new ArrayList();
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                outputs.add(line);
            }
            if (errStream.buffer.length() > 0) {
                return errStream.buffer.toString();
            }
            return outputs.toArray(new String[outputs.size()]);
        } catch (IOException e) {
            return e.getMessage();
        }
    }

    private class StdWorker extends SwingWorker {
        private InputStream stream;
        private StringBuffer buffer;

        public StdWorker(InputStream stream) {
            this.stream = stream;
            this.buffer = new StringBuffer();
        }

        @Override
        protected Object doInBackground() throws Exception {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                this.buffer.append(line + "\n");
            }
            return null;
        }
    }

    public interface FinishListener {
        void end(String[] outputs);

        void fail(String msg);
    }

}
