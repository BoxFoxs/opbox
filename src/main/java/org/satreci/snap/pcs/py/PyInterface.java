package org.satreci.snap.pcs.py;

import org.satreci.snap.pcs.preferences.PCSOptionsUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class PyInterface {

    public static ProcessWrapper createProcessBuilder(String pyName, Object... args) {
        pyName = (pyName.endsWith(".py") ? pyName : pyName + ".py");
        if (!new File(PCSOptionsUtil.getWorkingDir() + pyName).exists()) {
            install();
        }
        String[] argments = new String[2 + args.length];
        argments[0] = "python";
        argments[1] = pyName;
        for (int i = 2; i < argments.length; i++) {
            if (args[i - 2] == null) {
                argments[i] = "null";
                continue;
            }
            if (args[i - 2] instanceof File)
                argments[i] = ((File) args[i - 2]).getPath();
            else
                argments[i] = args[i - 2].toString();
        }
        ProcessBuilder pb = new ProcessBuilder(argments);
        pb.directory(new File(PCSOptionsUtil.getWorkingDir()));
        return new ProcessWrapper(pb);
    }

    public static synchronized boolean install() {
        boolean result = false;
        try {
            File dir = new File(PCSOptionsUtil.getWorkingDir());
            if (!dir.exists())
                dir.mkdirs();
            decompress(new URL("http://192.168.30.70:8089/thredds/fileServer/KOSC/PCS/opbox-python.zip").openStream(), PCSOptionsUtil.getWorkingDir());
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void decompress(InputStream zipFileStream, String directory) throws IOException {
        ZipInputStream zis = null;
        ZipEntry zipentry = null;
        try {
            zis = new ZipInputStream(zipFileStream);
            while ((zipentry = zis.getNextEntry()) != null) {
                String filename = zipentry.getName();
                File file = new File(directory, filename);
                if (zipentry.isDirectory()) {
                    file.mkdirs();
                } else {
                    createFile(file, zis);
                }
            }
        } catch (IOException e) {
            throw e;
        } finally {
            if (zis != null)
                zis.close();
            if (zipFileStream != null)
                zipFileStream.close();
        }
    }

    private static void createFile(File file, ZipInputStream zis) throws IOException {
        File parentDir = new File(file.getParent());
        if (!parentDir.exists()) {
            parentDir.mkdirs();
        }

        try (FileOutputStream fos = new FileOutputStream(file)) {
            byte[] buffer = new byte[256];
            int size = 0;
            while ((size = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, size);
            }
        } catch (IOException e) {
            throw e;
        }
    }


}

