package org.satreci.snap.pcs.py;

import org.esa.snap.rcp.SnapApp;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * Dialog which displays indeterminate progress.
 *
 * @author <a href="mailto:jclasslib@ej-technologies.com">Ingo Kegel</a>
 * @version $Revision: 1.1 $ $Date: 2003/08/18 07:46:43 $
 */
public class ProgressDialog extends JDialog {

    private static final int PROGRESS_BAR_WIDTH = 200;

    private JProgressBar progressBar;
    private JLabel lblMessage;

    public ProgressDialog(Frame parent, String title, String message) {
        super(parent);
        init(title, message);
    }

    public void setMessage(String message) {
        lblMessage.setText(message);
    }


    public void setVisible(boolean visible) {
        if (visible) {
            progressBar.setIndeterminate(true);
        } else {
            progressBar.setIndeterminate(false);
        }
        super.setVisible(visible);
    }

    private void init(String title, String message) {
        setTitle(title);
        setupControls();
        setupComponent();
        setMessage(message);
    }

    private void setupControls() {

        progressBar = new JProgressBar();
        Dimension preferredSize = progressBar.getPreferredSize();
        preferredSize.width = PROGRESS_BAR_WIDTH;
        progressBar.setPreferredSize(preferredSize);
        lblMessage = new JLabel(" ");
    }

    private void setupComponent() {

        JPanel contentPane = (JPanel) getContentPane();
        contentPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();
        gc.gridx = 0;
        gc.gridy = GridBagConstraints.RELATIVE;
        gc.anchor = GridBagConstraints.NORTHWEST;
        contentPane.add(lblMessage, gc);
        gc.weightx = 1;
        gc.fill = GridBagConstraints.HORIZONTAL;
        contentPane.add(progressBar, gc);
        setModal(false);
        pack();
        setLocationRelativeTo(getParent());

    }

}