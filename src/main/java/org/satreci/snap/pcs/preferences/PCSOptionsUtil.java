package org.satreci.snap.pcs.preferences;

import org.esa.snap.runtime.Config;

public class PCSOptionsUtil {
    public static final String pathPcsWorkingDirectory = "optbx.pcs.pathPCSWorkingSpace";

    public static final String DEFAULT_WORKING_DIR = System.getProperty("user.home") + "/PCS_Workingspace/";

    public static String getWorkingDir() {
        return Config.instance().preferences().get(pathPcsWorkingDirectory, DEFAULT_WORKING_DIR);
    }
}
