package org.satreci.snap.pcs.preferences;

import com.bc.ceres.swing.TableLayout;
import org.esa.snap.rcp.SnapApp;
import org.esa.snap.runtime.Config;
import org.openide.util.NbBundle;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.io.File;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class OPOptionsPanel extends JPanel {
    private JTextField pathField;
    private JButton findPathButton;

    OPOptionsPanel(final OPTBXOptionsPanelController controller) {
        initComponents();
        pathField.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                controller.changed();
            }

            public void removeUpdate(DocumentEvent e) {
                controller.changed();
            }

            public void insertUpdate(DocumentEvent e) {
                controller.changed();
            }
        });
    }

    private void initComponents() {
        //Main panel
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        //Combos Panel
        TableLayout tableLayout = new TableLayout(3);
        tableLayout.setTableAnchor(TableLayout.Anchor.NORTHWEST);
        tableLayout.setTablePadding(new Insets(4, 10, 0, 0));
        tableLayout.setTableFill(TableLayout.Fill.BOTH);
        tableLayout.setColumnWeightX(1, 2.0);

        JPanel comboPanel = new JPanel(tableLayout);
        pathField = new JTextField();
        pathField.setEditable(false);
        findPathButton = new JButton(NbBundle.getMessage(OPOptionsPanel.class,
                "OPTBXPCSOptionPanel.findPCSWorkingDir.text"));
        findPathButton.addActionListener((event) -> {
            showDirectoryChooser();
        });
        comboPanel.add(new JLabel(NbBundle.getMessage(OPOptionsPanel.class,
                "OPTBXPCSOptionPanel.guidePCSWorkingDir.text")));
        comboPanel.add(pathField);
        comboPanel.add(findPathButton);
        comboPanel.add(tableLayout.createHorizontalSpacer());
        comboPanel.add(tableLayout.createVerticalSpacer());
        this.add(comboPanel, BorderLayout.NORTH);
    }

    private void showDirectoryChooser() {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            String path = chooser.getSelectedFile().getPath();
            this.pathField.setText(path + "/");
        }
    }

    void load() {
        pathField.setText(PCSOptionsUtil.getWorkingDir());
    }

    void store() {
        final Preferences preferences = Config.instance().preferences();
        preferences.put(PCSOptionsUtil.pathPcsWorkingDirectory, pathField.getText());
        if (!new File(pathField.getText()).exists())
            new File(pathField.getText()).mkdirs();
        try {
            preferences.flush();
        } catch (BackingStoreException e) {
            SnapApp.getDefault().getLogger().severe(e.getMessage());
        }
    }

    boolean valid() {
        // Check whether form is consistent and complete
        return true;
    }

}
