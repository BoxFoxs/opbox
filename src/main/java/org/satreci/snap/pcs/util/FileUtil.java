package org.satreci.snap.pcs.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FileUtil {

    public static void copyFile(File original, File dest) {
        try {
            if (!dest.getParentFile().exists())
                dest.createNewFile();
            FileInputStream fis = new FileInputStream(original);
            FileOutputStream fos = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer)) > 0) {
                fos.write(buffer, 0, length);
            }
            fis.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String removeExtension(String fileName) {
        return fileName.substring(0, fileName.lastIndexOf("."));
    }

    public static String replaceExtension(String fileName, String ext) {
        return removeExtension(fileName) + "." + ext;
    }

    public static void deleteFile(File file) {
        file.delete();
    }
}
