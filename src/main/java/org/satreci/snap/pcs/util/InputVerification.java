package org.satreci.snap.pcs.util;

import java.io.File;

public class InputVerification {
    public static boolean checkDCCDark2Directory(File file) {
        if (file != null) {

            return true;
        }
        return false;
    }

    public static boolean checkL0Product(File file) {
        if (file != null) {

            return true;
        }
        return false;
    }

    public static boolean checkNotNull(Object... args) {
        for (Object arg : args) {
            if (arg == null)
                return false;
        }
        return true;
    }
}
