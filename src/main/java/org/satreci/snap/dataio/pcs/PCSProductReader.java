package org.satreci.snap.dataio.pcs;

import com.bc.ceres.core.ProgressMonitor;
import org.esa.snap.core.dataio.AbstractProductReader;
import org.esa.snap.core.dataio.ProductReaderPlugIn;
import org.esa.snap.core.datamodel.Band;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.datamodel.ProductData;
import org.esa.snap.core.util.Guardian;
import org.esa.snap.core.util.io.FileUtils;
import org.satreci.snap.dataio.bin.BinaryAttributeForm;
import org.satreci.snap.dataio.bin.BinaryPixelReader;
import org.satreci.snap.dataio.bin.ProductSize;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PCSProductReader extends AbstractProductReader {
    public static final String PRODUCT_TYPE = "GOCI-II PCS";
    private int sceneRasterWidth, sceneRasterHeight;
    private String productName;
    private List<Band> bandList;
    private Map<Band, BinaryPixelReader> bandReaderMap;


    private int size, bandType;
    private ByteOrder byteOrder;

    public PCSProductReader(String productName, int width, int height, ProductSize productSize, ByteOrder order) {
        super(null);
        this.productName = productName;
        this.sceneRasterWidth = width;
        this.sceneRasterHeight = height;
        this.size = productSize.getSize();
        this.bandType = productSize.getType();
        this.byteOrder = order;
        this.bandList = new ArrayList<>();
        this.bandReaderMap = new HashMap<>();
    }

    public BinaryPixelReader[] getBandReaders() {
        BinaryPixelReader[] readers = new BinaryPixelReader[bandReaderMap.size()];
        int idx = 0;
        for (Object key : bandReaderMap.keySet()) {
            readers[idx++] = bandReaderMap.get(key);
        }
        return readers;
    }

    public BinaryPixelReader getBandReader(Band band) {
        return bandReaderMap.get(band);
    }

    @Override
    protected Product readProductNodesImpl() throws IOException {
        Product product = new Product(productName, PRODUCT_TYPE, sceneRasterWidth, sceneRasterHeight, this);
        for (Band band : bandList)
            product.addBand(band);

        return product;
    }

    public void addBand(File file) throws FileNotFoundException {
        Band band = new Band(file.getName(), bandType, sceneRasterWidth, sceneRasterHeight);
        band.setSpectralBandIndex(bandList.size());
        bandReaderMap.put(band, new BinaryPixelReader(file, size, byteOrder, false));
        bandList.add(band);
    }

    @Override
    protected synchronized void readBandRasterDataImpl(int sourceOffsetX, int sourceOffsetY, int sourceWidth, int sourceHeight,
                                                       int sourceStepX, int sourceStepY, Band targetBand, int targetOffsetX, int targetOffsetY, int targetWidth,
                                                       int targetHeight, ProductData rasterData, ProgressMonitor pm) throws IOException {
        Guardian.assertNotNull("rasterData", rasterData);
        Guardian.assertNotNull("rasterData", targetBand);
        try {
            bandReaderMap.get(targetBand).readBandRasterData(targetBand, rasterData, sourceOffsetX, sourceOffsetY, sourceWidth, sourceHeight, targetWidth, targetHeight);
        } finally {
            pm.done();
        }
    }
}
