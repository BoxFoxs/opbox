package org.satreci.snap.dataio.pcs;

import com.bc.ceres.core.ProgressMonitor;
import org.esa.snap.core.dataio.AbstractProductWriter;
import org.esa.snap.core.dataio.ProductWriterPlugIn;
import org.esa.snap.core.datamodel.Band;
import org.esa.snap.core.datamodel.ProductData;
import org.satreci.snap.dataio.bin.BinaryPixelReader;
import org.satreci.snap.pcs.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PCSProductWriter extends AbstractProductWriter {
    private List<File> outputFiles;

    public PCSProductWriter(ProductWriterPlugIn writerPlugIn) {
        super(writerPlugIn);
        outputFiles = new ArrayList<>();
    }

    @Override
    protected void writeProductNodesImpl() throws IOException {
        final Object output = getOutput();

        File outputFile = null;
        if (output instanceof String) {
            outputFile = new File((String) output);
        } else if (output instanceof File) {
            outputFile = (File) output;
        }
        outputFile = initDirs(outputFile);

        getSourceProduct().setProductWriter(this);
        PCSProductReader reader = (PCSProductReader) getSourceProduct().getProductReader();
        writeBands(reader.getBandReaders(), outputFile);
    }

    private void writeBands(BinaryPixelReader[] readers, File targetDir) {
        for (BinaryPixelReader reader : readers) {
            FileUtil.copyFile(reader.getFile(), new File(targetDir.getPath() + "/" + reader.getFile().getName()));
        }
    }

    private File initDirs(File file) {
        File dirs = new File(FileUtil.removeExtension(file.getPath()));
        dirs.mkdirs();
        return dirs;
    }

    @Override
    public void writeBandRasterData(Band band, int i, int i1, int i2, int i3, ProductData productData, ProgressMonitor progressMonitor) throws IOException {
    }

    @Override
    public void flush() throws IOException {

    }

    @Override
    public void close() throws IOException {

    }

    @Override
    public void deleteOutput() throws IOException {
        flush();
        close();
        for (File outputFile : outputFiles)
            if (outputFile != null && outputFile.exists() && outputFile.isFile()) {
                outputFile.delete();
            }
    }
}
