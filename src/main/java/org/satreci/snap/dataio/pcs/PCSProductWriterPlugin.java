package org.satreci.snap.dataio.pcs;

import org.esa.snap.core.dataio.EncodeQualification;
import org.esa.snap.core.dataio.ProductWriter;
import org.esa.snap.core.dataio.ProductWriterPlugIn;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.util.io.SnapFileFilter;

import java.io.File;
import java.util.Locale;

public class PCSProductWriterPlugin implements ProductWriterPlugIn {
    private static final String FORMAT_NAME = "PCS Product";

    @Override
    public EncodeQualification getEncodeQualification(Product product) {
        if (product.getProductReader() instanceof PCSProductReader)
            return new EncodeQualification(EncodeQualification.Preservation.FULL);
        return new EncodeQualification(EncodeQualification.Preservation.UNABLE);
    }

    @Override
    public ProductWriter createWriterInstance() {
        return new PCSProductWriter(this);
    }

    @Override
    public Class[] getOutputTypes() {
        return new Class[]{String.class, File.class};
    }

    @Override
    public String[] getFormatNames() {
        return new String[]{FORMAT_NAME};
    }

    @Override
    public String[] getDefaultFileExtensions() {
        return new String[]{".pxl", ".hdr", ".bin"};
    }

    @Override
    public String getDescription(Locale locale) {
        return "GOCI-II PCS product writer";
    }

    @Override
    public SnapFileFilter getProductFileFilter() {
        return new SnapFileFilter(getFormatNames()[0], getDefaultFileExtensions(), getDescription(null));
    }
}
