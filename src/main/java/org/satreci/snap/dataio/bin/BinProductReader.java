package org.satreci.snap.dataio.bin;

import com.bc.ceres.core.ProgressMonitor;
import org.esa.snap.core.dataio.*;
import org.esa.snap.core.datamodel.Band;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.datamodel.ProductData;
import org.esa.snap.core.util.Guardian;
import org.esa.snap.core.util.io.FileUtils;
import org.esa.snap.rcp.layermanager.layersrc.product.ProductLayerSource;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class BinProductReader extends AbstractProductReader {
    private static final String BINARY = "BINARY";
    private BinaryPixelReader reader;
    private int sceneRasterWidth, sceneRasterHeight;
    private List<Band> bandList;

    private boolean prepared;

    private int size, bandType, bandCount;
    private ByteOrder byteOrder;

    public BinProductReader(ProductReaderPlugIn readerPlugIn, int width, int height, ProductSize productSize, int bandCount, ByteOrder order) {
        super(readerPlugIn);
        initBandReader(height, width, productSize.getSize(), productSize.getType(), bandCount, order);
    }

    public BinProductReader(ProductReaderPlugIn readerPlugIn) {
        super(readerPlugIn);
    }

    public void initBandReader(int height, int width, int size, int bandType, int bandCount, ByteOrder byteOrder) {
        this.sceneRasterWidth = width;
        this.sceneRasterHeight = height;
        this.size = size;
        this.bandType = bandType;
        this.bandCount = bandCount;
        this.byteOrder = byteOrder;
        this.prepared = true;
    }

    protected File getInputFile() {
        File inFile;
        if (getInput() instanceof String) {
            inFile = new File((String) getInput());
        } else if (getInput() instanceof File) {
            inFile = (File) getInput();
        } else {
            throw new IllegalArgumentException("unsupported input source: " + getInput());
        }
        return inFile;
    }


    @Override
    protected Product readProductNodesImpl() throws IOException {
        if (!prepared) new BinaryAttributeForm(this).show();
        if (!prepared)
            return null;
        File inFile = getInputFile();
        String name = FileUtils.getFilenameWithoutExtension(inFile.getName());
        Product product = new Product(name, BINARY, sceneRasterWidth, sceneRasterHeight, this);
        product.setFileLocation(inFile);

        initBandlist();
        for (Band band : bandList)
            product.addBand(band);

        return product;
    }

    private void initBandlist() {
        bandList = new ArrayList<Band>();
        for (int i = 0; i < bandCount; i++) {
            Band band = new Band(getInputFile().getName() + (i + 1), bandType, sceneRasterWidth, sceneRasterHeight);
            band.setSpectralBandIndex(i);
            band.setDescription(MessageFormat.format("pixel band by GOCI L1A {0}", i + 1));
            bandList.add(band);
        }
        try {
            FileChannel fc = new RandomAccessFile(getInputFile(), "r").getChannel();
            this.reader = new BinaryPixelReader(fc, size, byteOrder);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected synchronized void readBandRasterDataImpl(int sourceOffsetX, int sourceOffsetY, int sourceWidth, int sourceHeight,
                                                       int sourceStepX, int sourceStepY, Band targetBand, int targetOffsetX, int targetOffsetY, int targetWidth,
                                                       int targetHeight, ProductData rasterData, ProgressMonitor pm) throws IOException {
        Guardian.assertNotNull("rasterData", rasterData);
        Guardian.assertNotNull("rasterData", targetBand);
        try {
            reader.readBandRasterData(targetBand, rasterData, sourceOffsetX, sourceOffsetY, sourceWidth, sourceHeight, targetWidth, targetHeight);
        } finally {
            pm.done();
        }
    }
}
