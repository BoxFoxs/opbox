package org.satreci.snap.dataio.bin;

import org.esa.snap.core.dataio.DecodeQualification;
import org.esa.snap.core.dataio.ProductReader;
import org.esa.snap.core.dataio.ProductReaderPlugIn;
import org.esa.snap.core.util.io.FileUtils;
import org.esa.snap.core.util.io.SnapFileFilter;

import java.io.File;
import java.util.Locale;

public class BinProductReaderPlugin implements ProductReaderPlugIn {
    private static final String[] formats = new String[]{"bin", "pxl"};

    @Override
    public String[] getDefaultFileExtensions() {
        return new String[]{"bin", "pxl"};
    }

    @Override
    public String getDescription(Locale arg0) {
        return "Binary product reader";
    }

    @Override
    public String[] getFormatNames() {
        return new String[]{"Binary product"};
    }

    @Override
    public SnapFileFilter getProductFileFilter() {
        String[] formatNames = getFormatNames();
        String formatName = "";
        if (formatNames.length > 0) {
            formatName = formatNames[0];
        }
        return new SnapFileFilter(formatName, getDefaultFileExtensions(), getDescription(null));
    }

    @Override
    public ProductReader createReaderInstance() {
        return new BinProductReader(this);
    }

    @Override
    public DecodeQualification getDecodeQualification(Object input) {
        final File file;
        if (input instanceof String) {
            file = new File((String) input);
        } else if (input instanceof File) {
            file = (File) input;
        } else {
            return DecodeQualification.UNABLE;
        }
        String format = FileUtils.getExtension(file);
        boolean checkDecodeable = false;
        for (String formatName : formats) {
            if (format.contains(formatName)) {
                checkDecodeable = true;
            }
        }
        if (checkDecodeable) {
            return DecodeQualification.INTENDED;
        }
        return DecodeQualification.UNABLE;
    }

    @Override
    public Class[] getInputTypes() {
        return new Class[]{String.class, File.class};
    }
}
