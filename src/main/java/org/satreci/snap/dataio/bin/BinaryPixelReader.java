package org.satreci.snap.dataio.bin;

import org.esa.snap.core.datamodel.Band;
import org.esa.snap.core.datamodel.ProductData;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;

public class BinaryPixelReader {
    private int size;
    private FileChannel fc;
    private ByteOrder byteOrder;
    private File file;
    private boolean multiBand;

    public BinaryPixelReader(FileChannel fc, int size, ByteOrder byteOrder) throws FileNotFoundException {
        this(fc, size, byteOrder, true);
    }

    public BinaryPixelReader(File file, int size, ByteOrder byteOrder, boolean multiBand) throws FileNotFoundException {
        this(new RandomAccessFile(file, "r").getChannel(), size, byteOrder, multiBand);
        this.file = file;
    }

    public BinaryPixelReader(FileChannel fc, int size, ByteOrder byteOrder, boolean multiBand) throws FileNotFoundException {
        this.fc = fc;
        this.size = size;
        this.byteOrder = byteOrder;
        this.multiBand = multiBand;
    }

    public File getFile() {
        return file;
    }

    public void readBandRasterData(Band targetBand, ProductData rasterData, int sourceOffsetX, int sourceOffsetY, int sourceWidth, int sourceHeight, int targetWidth, int targetHeight) throws IOException {

        int offset = 0;
        if (multiBand)
            offset = (targetBand.getRasterHeight() * targetBand.getRasterWidth() * size) * targetBand.getSpectralBandIndex();
        final int sceneRasterWidth = targetBand.getRasterWidth();
        if (sourceOffsetX == 0 && sourceWidth == sceneRasterWidth && rasterData.getNumElems() == sourceWidth * sourceHeight) {
            long pos = offset + sourceOffsetY * sceneRasterWidth * size;
            rasterData.readFrom(getDataAsStream(pos, rasterData.getNumElems()));
        } else if (targetWidth == sourceWidth || targetHeight == sourceHeight) {
            for (int i = 0; i < sourceHeight; i++) {
                long pos = offset + ((i + sourceOffsetY) * sceneRasterWidth + sourceOffsetX) * size;
                rasterData.readFrom(i * sourceWidth, sourceWidth, getDataAsStream(pos, sourceWidth));
            }
        }
    }

    public ImageInputStream getDataAsStream(long pos, int length) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(length * size);
        fc.position(pos);
        fc.read(buffer);
        buffer.rewind();
        ImageInputStream stream = ImageIO.createImageInputStream(new ByteArrayInputStream(buffer.array()));
        stream.setByteOrder(this.byteOrder);
        return stream;
    }
}