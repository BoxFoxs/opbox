package org.satreci.snap.dataio.bin;

import org.esa.snap.core.datamodel.ProductData;
import org.esa.snap.rcp.SnapApp;
import org.esa.snap.ui.ModalDialog;

import javax.swing.*;
import java.awt.*;
import java.nio.ByteOrder;

public class BinaryAttributeForm extends ModalDialog {
    private BinProductReader reader;
    private JSpinner widthTextField, heightTextField, bandCountTextField;
    private JComboBox<ProductSize> sizeChooser;
    private JComboBox<ByteOrderItem> byteOrderChooser;



    enum ByteOrderItem {
        LITTLE_ENDIAN(ByteOrder.LITTLE_ENDIAN), BIG_ENDIAN(ByteOrder.BIG_ENDIAN);
        final private ByteOrder order;

        ByteOrderItem(ByteOrder order) {
            this.order = order;
        }

        public ByteOrder getOrder() {
            return this.order;
        }
    }

    public BinaryAttributeForm(BinProductReader reader) {
        super(SnapApp.getDefault().getMainFrame(), "Binary Product reader", ModalDialog.ID_OK_CANCEL, null);
        this.reader = reader;
        setContent(createPanel());
    }

    private JComponent createPanel() {
        final JPanel pane = new JPanel();
        pane.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        pane.add(new JLabel("width : "), c);

        SpinnerModel model = new SpinnerNumberModel(2720, 1, Integer.MAX_VALUE, 1);
        widthTextField = new JSpinner(model);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 0;
        pane.add(widthTextField, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 1;
        pane.add(new JLabel("height : "), c);

        SpinnerModel model2 = new SpinnerNumberModel(2720, 1, Integer.MAX_VALUE, 1);
        heightTextField = new JSpinner(model2);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 1;
        pane.add(heightTextField, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 2;
        pane.add(new JLabel("band : "), c);

        SpinnerModel model3 = new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1);
        bandCountTextField = new JSpinner(model3);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 2;
        pane.add(bandCountTextField, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 3;
        pane.add(new JLabel("data type : "), c);

        sizeChooser = new JComboBox<>(ProductSize.values());
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 3;
        pane.add(sizeChooser, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 4;
        pane.add(new JLabel("byteorder : "), c);

        byteOrderChooser = new JComboBox<>(ByteOrderItem.values());
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 4;
        pane.add(byteOrderChooser, c);

        return pane;
    }


    @Override
    protected void onOK() {
        int height = (int) (heightTextField.getModel()).getValue();
        int width = (int) (widthTextField.getModel()).getValue();
        ProductSize selectedSize = (ProductSize) sizeChooser.getSelectedItem();
        ByteOrder byteOrder = ((ByteOrderItem) byteOrderChooser.getSelectedItem()).getOrder();
        int bandCount = (int) bandCountTextField.getModel().getValue();
        reader.initBandReader(height, width, selectedSize.getSize(), selectedSize.getType(), bandCount, byteOrder);
        hide();
    }
}
