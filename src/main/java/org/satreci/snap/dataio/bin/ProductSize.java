package org.satreci.snap.dataio.bin;

import org.esa.snap.core.datamodel.ProductData;

public enum ProductSize {
    INT(Integer.SIZE, ProductData.TYPE_INT32), SHORT(Short.SIZE, ProductData.TYPE_INT16), LONG(Long.SIZE, ProductData.TYPE_INT64), BYTE(Byte.SIZE, ProductData.TYPE_INT8),
    UINT(Integer.SIZE, ProductData.TYPE_UINT32), USHORT(Short.SIZE, ProductData.TYPE_UINT16), UBYTE(Byte.SIZE, ProductData.TYPE_UINT8), FLOAT(Float.SIZE, ProductData.TYPE_FLOAT32), DOUBLE(Double.SIZE, ProductData.TYPE_FLOAT64);

    final private int size, type;

    ProductSize(int size, int type) {
        this.size = size / 8;
        this.type = type;
    }

    public int getSize() { // 문자를 받아오는 함수
        return size;
    }

    public int getType() {
        return type;
    }
}