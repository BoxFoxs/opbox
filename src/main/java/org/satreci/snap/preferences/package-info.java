@OptionsPanelController.ContainerRegistration(
        id = "OPTBX",
        categoryName = "#LBL_OPTBXOptionsCategory_Name",
        iconBase = "org/satreci/snap/preferences/icon.png",
        keywords = "#LBL_OPTBXOptionsCategory_Keywords",
        keywordsCategory = "OPTBX",
        position = 1000
)
@NbBundle.Messages(value = {
        "LBL_OPTBXOptionsCategory_Name=OPTBX",
        "LBL_OPTBXOptionsCategory_Keywords=optbx,sar,goci"
})
package org.satreci.snap.preferences;

import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.NbBundle;